import { ConflictException, Inject, Injectable } from '@nestjs/common'
import { createId } from '@paralleldrive/cuid2'
import { DB, Drizzle, eq, schema } from 'src/db'
import { UserProfile, User } from 'src/db/schema/users'
import { head } from 'src/util/head'

@Injectable()
export class UsersService {
    constructor(@Inject(DB) private db: Drizzle) {}

    async findOne(username: string): Promise<User | undefined> {
        return await this.db.query.users.findFirst({
            where: eq(schema.users.username, username)
        })
    }

    async createOne(username: string, password: string): Promise<UserProfile> {
        try {
            return await this.db
                .insert(schema.users)
                .values({ id: createId(), username, password })
                .returning({
                    id: schema.users.id,
                    username: schema.users.password
                })
                .then(head)
        } catch {
            throw new ConflictException('Username is already taken')
        }
    }
}
