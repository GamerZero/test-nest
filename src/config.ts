import { z } from 'zod'

export const configSchema = z.object({
    NODE_ENV: z.enum(['development', 'production']),
    CI: z.string().min(1).optional(),
    BIND_ADDRESS: z.string().min(1),
    PORT: z.string().min(1),
    DATABASE_URL: z.string().url(),
    ALLOWED_ORIGINS: z
        .string()
        .refine(
            urls => z.array(z.string().url()).safeParse(urls.split(',')).success
        ),
    COOKIE_DOMAIN: z.string().min(1),
    JWT_SECRET: z.string().min(1),
    MAX_HEALTHY_HEAP_MB: z.string().min(1)
})

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace NodeJS {
        interface ProcessEnv extends z.infer<typeof configSchema> {}
    }
}
