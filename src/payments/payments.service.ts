import { Inject, Injectable } from '@nestjs/common'
import { CreatePaymentDto } from './dto/create-payment.dto'
import { DB, Drizzle, and, desc, eq, schema } from 'src/db'
import { Payment } from 'src/db/schema/payments'
import { createId } from '@paralleldrive/cuid2'
import { head } from 'src/util/head'
import { ExamplePaymentProvider } from './example-payment-provider'
import { PaymentProvider } from './payment-provider'
import { ConfirmPaymentDto } from './dto/confirm-payment.dto'
import { FindAllDto } from './dto/find-all.dto'

@Injectable()
export class PaymentsService {
    private paymentProviders: Record<string, PaymentProvider> = {
        example: new ExamplePaymentProvider()
    }

    constructor(@Inject(DB) private db: Drizzle) {}

    async create(
        payerId: string,
        createPaymentDto: CreatePaymentDto
    ): Promise<{ payment: Payment; confirmationUrl: string }> {
        const payment = await this.db
            .insert(schema.payments)
            .values({
                id: createId(),
                payerId,
                amount: createPaymentDto.amount.toString()
            })
            .returning()
            .then(head)

        const providerPayment = await this.paymentProviders[
            createPaymentDto.provider
        ].createPayment(payment.id, createPaymentDto.amount)

        return { payment, confirmationUrl: providerPayment.confirmationUrl }
    }

    async findAll(
        forUserId: string,
        findAllDto: FindAllDto
    ): Promise<Payment[]> {
        return await this.db.query.payments.findMany({
            where: eq(schema.payments.payerId, forUserId),
            orderBy: desc(schema.payments.createdAt),
            limit: findAllDto.limit,
            offset: findAllDto.offset
        })
    }

    async confirmPayment(
        payerId: string,
        { paymentId, result }: ConfirmPaymentDto
    ): Promise<void> {
        await this.db
            .update(schema.payments)
            .set({ state: result })
            .where(
                and(
                    eq(schema.payments.id, paymentId),
                    eq(schema.payments.payerId, payerId)
                )
            )
    }
}
