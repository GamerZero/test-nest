import { Controller, Get, Post, Body, UseGuards, Query } from '@nestjs/common'
import { PaymentsService } from './payments.service'
import { CreatePaymentDto } from './dto/create-payment.dto'
import { AuthGuard } from 'src/auth/auth.guard'
import { ContextUser } from 'src/auth/auth.decorator'
import { UserProfile } from 'src/db/schema/users'
import { Payment } from 'src/db/schema/payments'
import { ConfirmPaymentDto } from './dto/confirm-payment.dto'
import { FindAllDto } from './dto/find-all.dto'
import { ApiQuery } from '@nestjs/swagger'

@Controller('payments')
export class PaymentsController {
    constructor(private readonly paymentsService: PaymentsService) {}

    @Post()
    @UseGuards(AuthGuard)
    create(
        @ContextUser() user: UserProfile,
        @Body() createPaymentDto: CreatePaymentDto
    ): Promise<{ payment: Payment; confirmationUrl: string }> {
        return this.paymentsService.create(user.id, createPaymentDto)
    }

    @Get()
    @ApiQuery({ name: 'limit', type: 'number', example: 5 })
    @ApiQuery({ name: 'offset', type: 'number', example: 0 })
    @UseGuards(AuthGuard)
    findAll(
        @ContextUser() user: UserProfile,
        @Query() findAllDto: FindAllDto
    ): Promise<Payment[]> {
        return this.paymentsService.findAll(user.id, findAllDto)
    }

    @Get('confirm-payment')
    @ApiQuery({ name: 'paymentId', type: 'string' })
    @ApiQuery({ name: 'result', type: 'string', example: 'succeeded' })
    @UseGuards(AuthGuard)
    async confirmPayment(
        @ContextUser() user: UserProfile,
        @Query() confirmPaymentDto: ConfirmPaymentDto
    ): Promise<void> {
        await this.paymentsService.confirmPayment(user.id, confirmPaymentDto)
    }
}
