import { Module } from '@nestjs/common'
import { PaymentsService } from './payments.service'
import { PaymentsController } from './payments.controller'
import { DbModule } from 'src/db/db.module'

@Module({
    imports: [DbModule],
    controllers: [PaymentsController],
    providers: [PaymentsService]
})
export class PaymentsModule {}
