import { createZodDto } from 'nestjs-zod'
import { z } from 'nestjs-zod/z'

export class ConfirmPaymentDto extends createZodDto(
    z.object({
        paymentId: z.string().min(1),
        result: z.enum(['succeeded', 'canceled'])
    })
) {}
