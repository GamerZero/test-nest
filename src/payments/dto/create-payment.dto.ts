import { createZodDto } from 'nestjs-zod'
import { z } from 'nestjs-zod/z'
import Decimal from 'decimal.js'

export class CreatePaymentDto extends createZodDto(
    z.object({
        amount: z
            .string()
            .min(1)
            .refine(str => {
                try {
                    const decimal = new Decimal(str)
                    return decimal.isPositive() && decimal.decimalPlaces() <= 2
                } catch {
                    return false
                }
            })
            .transform(str => new Decimal(str).toFixed(2)),
        provider: z.enum(['example'])
    })
) {}
