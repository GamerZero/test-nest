import { createZodDto } from 'nestjs-zod'
import { z } from 'nestjs-zod/z'

export class FindAllDto extends createZodDto(
    z.object({
        limit: z.coerce.number().positive(),
        offset: z.coerce.number().nonnegative()
    })
) {}
