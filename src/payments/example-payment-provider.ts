import { PaymentProvider } from './payment-provider'

export class ExamplePaymentProvider implements PaymentProvider {
    async createPayment(
        paymentId: string
    ): Promise<{ confirmationUrl: string }> {
        return {
            confirmationUrl: `${this.getBaseUrl()}/api/v1/payments/confirm-payment?paymentId=${paymentId}&result=succeeded`
        }
    }

    private getBaseUrl(): string {
        return process.env.COOKIE_DOMAIN === 'localhost'
            ? `http://${process.env.COOKIE_DOMAIN}:${process.env.PORT}`
            : `https://${process.env.COOKIE_DOMAIN}`
    }
}
