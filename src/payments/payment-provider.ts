export interface PaymentProvider {
    createPayment(
        paymentId: string,
        amount: string
    ): Promise<{ confirmationUrl: string }>
}
