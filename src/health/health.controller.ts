import { Controller, Get } from '@nestjs/common'
import {
    HealthCheck,
    HealthCheckResult,
    HealthCheckService,
    MemoryHealthIndicator
} from '@nestjs/terminus'

@Controller('health')
export class HealthController {
    private readonly maxHeapMemoryBytes =
        parseInt(process.env.MAX_HEALTHY_HEAP_MB) * 1024 * 1024

    constructor(
        private health: HealthCheckService,
        private memory: MemoryHealthIndicator
    ) {}

    @Get()
    @HealthCheck()
    async check(): Promise<HealthCheckResult> {
        return this.health.check([
            () => this.memory.checkHeap('memoryHeap', this.maxHeapMemoryBytes)
        ])
    }
}
