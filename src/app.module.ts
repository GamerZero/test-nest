import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { configSchema } from './config'
import { HealthModule } from './health/health.module'
import { AuthModule } from './auth/auth.module'
import { UsersModule } from './users/users.module'
import { APP_PIPE } from '@nestjs/core'
import { ZodValidationPipe } from 'nestjs-zod'
import { PaymentsModule } from './payments/payments.module'

@Module({
    imports: [
        ConfigModule.forRoot({
            validate: config => configSchema.parse(config)
        }),
        HealthModule,
        AuthModule,
        UsersModule,
        PaymentsModule
    ],
    providers: [
        {
            provide: APP_PIPE,
            useClass: ZodValidationPipe
        }
    ]
})
export class AppModule {}
