import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import {
    FastifyAdapter,
    NestFastifyApplication
} from '@nestjs/platform-fastify'
import fastifyCookie from '@fastify/cookie'
import { Logger, VersioningType } from '@nestjs/common'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { patchNestJsSwagger } from 'nestjs-zod'

async function bootstrap(): Promise<void> {
    const app = await NestFactory.create<NestFastifyApplication>(
        AppModule,
        new FastifyAdapter()
    )

    await app.register(fastifyCookie)
    app.enableCors({
        origin: process.env.ALLOWED_ORIGINS.split(','),
        credentials: true
    })

    app.setGlobalPrefix('api')
    app.enableVersioning({
        type: VersioningType.URI,
        defaultVersion: '1'
    })

    patchNestJsSwagger()

    const config = new DocumentBuilder()
        .setTitle('Test')
        .setDescription('Example payments API')
        .setVersion('1.0')
        .build()
    const document = SwaggerModule.createDocument(app, config)
    SwaggerModule.setup('api', app, document)

    const logger = new Logger('Startup')
    logger.log(`Listening on ${[process.env.BIND_ADDRESS]}:${process.env.PORT}`)

    await app.listen(process.env.PORT, process.env.BIND_ADDRESS)
}

bootstrap()
