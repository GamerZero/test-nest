import {
    Body,
    Controller,
    Post,
    HttpCode,
    HttpStatus,
    Res,
    Get,
    UseGuards
} from '@nestjs/common'
import { AuthService } from './auth.service'
import { SignInDto } from './dto/sign-in.dto'
import { SignUpDto } from './dto/sign-up.dto'
import { FastifyReply } from 'fastify'
import { UserProfile } from 'src/db/schema/users'
import { CookieSerializeOptions } from '@fastify/cookie'
import { AuthGuard } from './auth.guard'
import { ContextUser } from './auth.decorator'

@Controller('auth')
export class AuthController {
    private readonly tokenCookieName = 'access_token'
    private readonly cookieOptions: CookieSerializeOptions

    constructor(private authService: AuthService) {
        this.cookieOptions = {
            domain: process.env.COOKIE_DOMAIN,
            path: '/',
            secure: true,
            httpOnly: true,
            sameSite: 'none'
        }
    }

    @Post('sign-in')
    @HttpCode(HttpStatus.OK)
    async signIn(
        @Res({ passthrough: true }) response: FastifyReply,
        @Body() signInDto: SignInDto
    ): Promise<void> {
        const accessToken = await this.authService.signIn(
            signInDto.username,
            signInDto.password
        )

        const oneWeek = 604800
        response.setCookie(this.tokenCookieName, accessToken, {
            ...this.cookieOptions,
            maxAge: oneWeek
        })
    }

    @Post('sign-up')
    signUp(@Body() signUpDto: SignUpDto): Promise<UserProfile> {
        return this.authService.signUp(signUpDto.username, signUpDto.password)
    }

    @Post('sign-out')
    signOut(@Res({ passthrough: true }) response: FastifyReply): void {
        response.clearCookie(this.tokenCookieName, this.cookieOptions)
    }

    @Get('profile')
    @UseGuards(AuthGuard)
    clown(@ContextUser() user: UserProfile): UserProfile {
        return user
    }
}
