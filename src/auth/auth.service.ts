import { Injectable, UnauthorizedException } from '@nestjs/common'
import { UsersService } from '../users/users.service'
import { JwtService } from '@nestjs/jwt'
import * as argon2 from 'argon2'
import { UserProfile } from 'src/db/schema/users'

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) {}

    async signIn(username: string, password: string): Promise<string> {
        const user = await this.usersService.findOne(username)
        if (!user || !(await argon2.verify(user.password, password))) {
            throw new UnauthorizedException('Invalid username or password')
        }

        const payload = { sub: user.id, username: user.username }
        return this.jwtService.signAsync(payload)
    }

    async signUp(username: string, password: string): Promise<UserProfile> {
        const hashedPassword = await argon2.hash(password)
        return this.usersService.createOne(username, hashedPassword)
    }
}
