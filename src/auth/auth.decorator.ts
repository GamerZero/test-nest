import { ExecutionContext, createParamDecorator } from '@nestjs/common'
import { FastifyRequest } from 'fastify'
import { UserProfile } from 'src/db/schema/users'

export const ContextUser = createParamDecorator<any, any, UserProfile>(
    (_, ctx: ExecutionContext) =>
        ctx.switchToHttp().getRequest<FastifyRequest>().user
)
