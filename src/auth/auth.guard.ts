import {
    CanActivate,
    ExecutionContext,
    Injectable,
    UnauthorizedException
} from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { FastifyRequest } from 'fastify'
import { UserProfile } from 'src/db/schema/users'

declare module 'fastify' {
    interface FastifyRequest {
        user: UserProfile
    }
}

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private jwtService: JwtService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest<FastifyRequest>()
        const token = request.cookies['access_token']
        if (!token) {
            throw new UnauthorizedException(
                'Authentication token was not provided'
            )
        }

        try {
            const payload = await this.jwtService.verifyAsync(token)
            request['user'] = {
                id: payload.sub,
                username: payload.username
            }
        } catch {
            throw new UnauthorizedException('Authentication token is invalid')
        }

        return true
    }
}
