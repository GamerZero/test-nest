import { createZodDto } from 'nestjs-zod'
import { z } from 'nestjs-zod/z'

export class SignInDto extends createZodDto(
    z.object({
        username: z.string().min(1),
        password: z.string().min(1)
    })
) {}
