import { createZodDto } from 'nestjs-zod'
import { z } from 'nestjs-zod/z'

export class SignUpDto extends createZodDto(
    z.object({
        username: z.string().min(1),
        password: z.password().min(8)
    })
) {}
