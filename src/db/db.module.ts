import { Module } from '@nestjs/common'
import { drizzle } from 'drizzle-orm/postgres-js'
import { DB, schema } from '.'
import { migrate } from 'drizzle-orm/postgres-js/migrator'
import * as postgres from 'postgres'

export const runMigrations = async (): Promise<void> => {
    const migrationClient = postgres(process.env.DATABASE_URL, { max: 1 })
    await migrate(drizzle(migrationClient), {
        migrationsFolder: './src/db/drizzle'
    })
    await migrationClient.end()
}

@Module({
    providers: [
        {
            provide: DB,
            useFactory: () => {
                if (process.env.NODE_ENV === 'production' && !process.env.CI) {
                    void runMigrations()
                }

                return drizzle(postgres(process.env.DATABASE_URL), { schema })
            }
        }
    ],
    exports: [{ provide: DB, useExisting: DB }]
})
export class DbModule {}
