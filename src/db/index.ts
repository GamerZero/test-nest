import { PostgresJsDatabase } from 'drizzle-orm/postgres-js'
import * as users from './schema/users'
import * as payments from './schema/payments'

export const schema = {
    ...users,
    ...payments
}

export * from 'drizzle-orm'

export type Drizzle = PostgresJsDatabase<typeof schema>

export const DB = Symbol('DB')
