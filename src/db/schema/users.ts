import { InferSelectModel, relations } from 'drizzle-orm'
import { pgTable, text, varchar } from 'drizzle-orm/pg-core'
import { payments } from './payments'

export const users = pgTable('user', {
    id: varchar('id', { length: 255 }).primaryKey(),
    username: text('username').notNull().unique(),
    password: text('password').notNull()
})

export const userRelations = relations(users, ({ many }) => ({
    payments: many(payments)
}))

export type User = InferSelectModel<typeof users>

type Forbid<T extends object, ToExlude extends PropertyKey> = Omit<
    T,
    ToExlude
> &
    Partial<Record<ToExlude, undefined>>

export type UserProfile = Forbid<User, 'password'>
