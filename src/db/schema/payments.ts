import { InferSelectModel, relations } from 'drizzle-orm'
import {
    decimal,
    pgEnum,
    pgTable,
    timestamp,
    varchar
} from 'drizzle-orm/pg-core'
import { users } from './users'

export const paymentState = pgEnum('payment_state', [
    'pending',
    'succeeded',
    'canceled'
])

export const payments = pgTable('payment', {
    id: varchar('id', { length: 255 }).primaryKey(),
    payerId: varchar('payer_id', { length: 255 })
        .notNull()
        .references(() => users.id, { onDelete: 'cascade' }),
    amount: decimal('amount', { scale: 2 }).notNull(),
    state: paymentState('state').notNull().default('pending'),
    createdAt: timestamp('created_at', { withTimezone: true })
        .notNull()
        .defaultNow()
})

export const paymentRelations = relations(payments, ({ one }) => ({
    payer: one(users, { fields: [payments.payerId], references: [users.id] })
}))

export type Payment = InferSelectModel<typeof payments>
