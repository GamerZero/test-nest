FROM node:20.11.0
WORKDIR /usr/app/

COPY package.json pnpm-lock.yaml .
RUN yarn global add pnpm && pnpm i

COPY . .
RUN CI=true pnpm build

CMD ["node", "/usr/app/dist/src/main.js"]
